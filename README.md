# PMD onboarding September 2023

This is a MyBinder environment to give you the quickest look into how to implement your software in pyiron.


## Motivation

How often did you have to confront with problems like these?

- I run a simulation before but I cannot find the files anymore
- Unintentionally erased previous data because file name wasn't changed
- I have to launch multiple simulations with varying parameters
- I would like to share my workflow with my colleagues

## Content

### Variation I: Classical `ScriptJob`

Write your tool inside `calculation.ipynb`. This can be externally controlled via `submitter.ipynb`.


### Variation II: `Workflow` (under development)

You can write a function and wrap it in the decorator `function_node`. This already makes it a workflow component.
